package DZ.Page;

import DZ.Zakaz2;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class SendToBasket {

    private WebDriver driver;

    // Нажать Добавать в корзину
    @FindBy(xpath = "//*[text()='добавить в корзину']")
    private WebElement Basket;

    public SendToBasket(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public SendToBasket loadWait() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return this;
    }
    public SendToBasket setBasket() {
        Basket.click();
        return this;
    }

}
