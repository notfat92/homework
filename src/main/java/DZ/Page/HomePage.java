package DZ.Page;

import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class HomePage {


    private WebDriver driver;
    private String baseUrl = "https://homebrandofficial.ru/wear";

    @FindBy(xpath = "//input[@placeholder='Поиск']")
    private WebElement searchLong;

    @FindBy(xpath = "//*[@class='t-store__search-icon js-store-filter-search-btn']")
    private WebElement buttonSearch;

    @FindBy(xpath = "//*[text()='Лонгслив White&Green']")
    private WebElement product;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public HomePage openHomePage () {
        driver.get(baseUrl);
        return this;
    }

    public HomePage loadWait() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return this;
    }
    public HomePage enterSearchText (String text) {
        searchLong.sendKeys(text);
        return this;
    }
    public HomePage clickSearch () {
        buttonSearch.click();
        return this;
    }
    public HomePage checkName (String name, SoftAssertions softAssert) {
        softAssert.assertThat(product.getText()).as("Неправильное название").isEqualTo(name);
        return this;

    }

}