package DZ.Page;

import DZ.Zakaz2;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class ChoiseTshort {

    private WebDriver driver;
    private String baseUrl = "https://homebrandofficial.ru/wear";

    // Нажать на товар с названием “Футболка Оversize”
    @FindBy(xpath = "//div[@class='js-store-prod-name js-product-name t-store__card__title t-name t-name_md'][text()='Футболка Оversize']")
    private WebElement tshortOverSize;

    // Нажать Добвать в корзину
    @FindBy(xpath = "//*[@class='t706__carticon-img']")
    private WebElement ClickBusket;

    // Нажать Оформить заказ
    @FindBy(xpath = "//button[@class='t706__sidebar-continue t-btn']")
    private WebElement ChechoutTshort;


    public ChoiseTshort(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public ChoiseTshort openHomePage () {
        driver.get(baseUrl);
        return this;
    }
    public ChoiseTshort loadWait() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return this;

    }
        public ChoiseTshort clickTshortOverSize() {
            tshortOverSize.click();
            return this;
        }
        public ChoiseTshort setClickBusket () {
        ClickBusket.click();
        return  this;
        }
        public ChoiseTshort setCheckoutTshort () {
        ChechoutTshort.click();
        return this;
        }

}
