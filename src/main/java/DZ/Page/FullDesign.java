package DZ.Page;

import DZ.Zakaz2;
import okhttp3.Address;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class FullDesign {
    private WebDriver driver;

    // Ввод ФИО
    @FindBy(xpath = "//input[@id='input_1496239431201']")
    private WebElement fullName;

    // Ввод адреса
    @FindBy(xpath = "//textarea[@id='input_1630305196291']")
    private WebElement address;

    // Ввод номера
    @FindBy(xpath = "//input[@style='color: rgb(94, 94, 94);']")
    private WebElement Number;

    // Выбор города
    @FindBy(xpath = "//input[@class='searchbox-input js-tilda-rule t-input']")
    private WebElement Cityclick;
    @FindBy(xpath = "//input[@class='searchbox-input js-tilda-rule t-input']")
    private WebElement Cityclear;
    @FindBy(xpath = "//input[@class='searchbox-input js-tilda-rule t-input']")
    private WebElement City1;
    @FindBy(xpath = "//div[@data-full-name='Россия, Томская область, г Томск']")
    private WebElement City2;

    // Ввод ФИО 2
    @FindBy(xpath = "//input[@placeholder='Иванов Иван Иванович']")
    private WebElement fullName2;

    //Ввод адреса
    @FindBy(xpath = "//input[@name='tildadelivery-street']")
    private WebElement adress2;

    // Ввод дома
    @FindBy(xpath = "//input[@name='tildadelivery-house']")
    private WebElement House1;
    @FindBy(xpath = "//input[@name='tildadelivery-house']")
    private WebElement House2;
    @FindBy(xpath = "//input[@name='tildadelivery-house']")
    private WebElement House3;

    // Нажать кнопку ОФОРМИТЬ ЗАКАЗ
    @FindBy(xpath = "//button[@class='t-submit'][text()='ОФОРМИТЬ ЗАКАЗ']")
    private WebElement mainOrder;

    // Проверка номера 1
    @FindBy(xpath = "//div[text()='Укажите, пожалуйста, корректный номер телефона']")
    private WebElement number1;

    // Проверка номера 2
    @FindBy(xpath = "//p[text()='Укажите, пожалуйста, корректный номер телефона']")
    private WebElement number2;


    public FullDesign(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public FullDesign loadWait() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return this;
    }
    public FullDesign setFullName() {
        fullName.sendKeys("Зубенко Михаил Петрович");
        return this;
    }
    public FullDesign setAdress() {
        address.sendKeys("Томск ул. Ленина 10 кв. 1");
        return this;
    }
    public FullDesign setNumber() {
        Number.sendKeys("0000000000");
        return this;
    }
    public FullDesign setCityclick() {
        Cityclick.click();
        return this;
    }
    public FullDesign setCityclear() {
        Cityclear.clear();
        return this;
    }
    public FullDesign setCity1() {
        City1.sendKeys("Томск");
        return this;
    }
    public FullDesign setCity2() {
        City2.click();
        return this;
}
    public FullDesign setFullName2() {
        fullName2.sendKeys("Зубенко Михаил Петрович");
        return this;
    }
    public FullDesign setAdress2() {
        adress2.sendKeys("пл Ленина" + Keys.TAB);
        return this;
    }
    public FullDesign setHouse1() {
        House1.click();
        return this;
    }
    public FullDesign setHouse2() {
        House2.clear();
        return this;
    }
    public FullDesign setHouse3() {
        House3.sendKeys("д. 10" + Keys.TAB);
        return this;
    }
    public FullDesign setMainOrder() {
        mainOrder.sendKeys("д. 10" + Keys.TAB);
        return this;
    }
    public FullDesign checkNumber1 (String numberone, SoftAssertions softAssert) {
        softAssert.assertThat(number1.getText()).as("Неправильный номер").isEqualTo(numberone);
        return this;
    }
    public FullDesign checkNumber2 (String numbertwo, SoftAssertions softAssert) {
        softAssert.assertThat(number2.getText()).as("Неправильный номер").isEqualTo(numbertwo);
        return this;
    }

}
