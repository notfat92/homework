package DZ;

import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Long2 {

    // Поиск Лонгслива
    @FindBy(xpath = "//input[@placeholder='Поиск']")
    private WebElement SearchLong;

    // Нажать Поиск
    @FindBy(xpath = "//*[@class='t-store__search-icon js-store-filter-search-btn']")
    private WebElement ButtonSearch;

    // Проверка названия
    @FindBy(xpath = "//*[text()='Лонгслив White&Green']")
    private WebElement resultName;

    // Проверка на цену
    @FindBy(xpath = "//*[text()='2 800']")
    private WebElement priceRes;

    //Проверка на количество
    @FindBy(xpath = "//*[text()='1']")
    private WebElement searchResNum;

    private WebDriver driver;

    public Long2(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    // Поиск Лонгслива
    public Long2 setPoisk() {
        SearchLong.sendKeys("Лонгслив White&Green");
        return this;
    }
    // Нажать Поиск
    public Long2 setButtonSearch() {
        ButtonSearch.click();
        return this;
    }
    // Нажать Поиск
    public Long2 resultNameCheck(SoftAssertions softAssert, String resName) {
        softAssert.assertThat(resultName.getText()).as("Неправильное название")
                .isEqualToIgnoringCase(resName);
        return this;
    }
    // Проверка на цену
    public Long2 resultPriceCheck(SoftAssertions softAssert, String price) {
        softAssert.assertThat(priceRes.getText()).as("Неверная цена")
                .isEqualToIgnoringCase(price);
        return this;
    }
    // Проверка на количество
    public Long2 searchResNumCheck(SoftAssertions softAssert, String num) {
        softAssert.assertThat(searchResNum.getText()).as("Неверное количество")
                .isEqualToIgnoringCase(num);
        return this;
    }

}




