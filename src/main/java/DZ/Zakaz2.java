package DZ;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Zakaz2 {

    // Нажать на товар с названием “Футболка Оversize” и добавить в корзину
    @FindBy(xpath = "//div[@class='js-store-prod-name js-product-name t-store__card__title t-name t-name_md'][text()='Футболка Оversize']")
    private WebElement Tshort;
    @FindBy(xpath = "//*[text()='добавить в корзину']")
    private WebElement Korzina;
    @FindBy(xpath = "//*[@class='t706__carticon-img']")
    private WebElement Dobavit;

    // Нажать ОФОРМИТЬ ЗАКАЗ
    @FindBy(xpath = "//button[@class='t706__sidebar-continue t-btn']")
    private WebElement Oformit;

    // Ввод ФИО и адреса
    @FindBy(xpath = "//input[@id='input_1496239431201']")
    private WebElement FIO1;
    @FindBy(xpath = "//textarea[@id='input_1630305196291']")
    private WebElement Adres1;

    //Ввод номера телефона
    @FindBy(xpath = "//input[@style='color: rgb(94, 94, 94);']")
    private WebElement Number;

    // Выбор города
    @FindBy(xpath = "//input[@class='searchbox-input js-tilda-rule t-input']")
    private WebElement Gorodclick;
    @FindBy(xpath = "//input[@class='searchbox-input js-tilda-rule t-input']")
    private WebElement Gorodclear;
    @FindBy(xpath = "//input[@class='searchbox-input js-tilda-rule t-input']")
    private WebElement Gorod1;
    @FindBy(xpath = "//div[@data-full-name='Россия, Томская область, г Томск']")
    private WebElement Gorod2;

    // Ввод ФИО 2
    @FindBy(xpath = "//input[@placeholder='Иванов Иван Иванович']")
    private WebElement FIO2;

    //Ввод адреса
    @FindBy(xpath = "//input[@name='tildadelivery-street']")
    private WebElement Adres2;

    // Ввод дома
    @FindBy(xpath = "//input[@name='tildadelivery-house']")
    private WebElement Dom1;
    @FindBy(xpath = "//input[@name='tildadelivery-house']")
    private WebElement Dom2;
    @FindBy(xpath = "//input[@name='tildadelivery-house']")
    private WebElement Dom3;

    // Нажать н кнопку ОФОРМИТЬ ЗАКАЗ
    @FindBy(xpath = "//button[@class='t-submit'][text()='ОФОРМИТЬ ЗАКАЗ']")
    private WebElement Oformit2;


    private WebDriver driver;

    public Zakaz2(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    // Поиск Лонгслива
    public Zakaz2 setTshort() {
        Tshort.click();
        return this;
    }// Поиск Лонгслива

    public Zakaz2 setKorzina() {
        Korzina.click();
        return this;
    }// Поиск Лонгслива

    public Zakaz2 setDobavit() {
        Dobavit.click();
        return this;
    }

    // Нажать ОФОРМИТЬ ЗАКАЗ
    public Zakaz2 setOformit() {
        Oformit.click();
        return this;
    }

    // Ввод ФИО
    public Zakaz2 setFIO1() {
        FIO1.sendKeys("Зубенко Михаил Петрович");
        return this;
    }

    // Ввод адреса
    public Zakaz2 setAdres1() {
        Adres1.sendKeys("Томск ул. Ленина 10 кв. 1");
        return this;
    }
    //Ввод номера телефона
    public Zakaz2 setNumber() {
        Number.sendKeys("0000000000");
        return this;
    }
    // Выбор города
    public Zakaz2 setGorodclick() {
        Gorodclick.click();
        return this;
    }
    // Выбор города
    public Zakaz2 setGorodclear() {
        Gorodclear.clear();
        return this;
    }
    // Выбор города
    public Zakaz2 setGorod1() {
        Gorod1.sendKeys("Томск");
        return this;
    }
    // Выбор города
    public Zakaz2 setGorod2() {
        Gorod2.click();
        return this;
    }
    // Ввод ФИО 2
    public Zakaz2 setFIO2() {
        FIO2.sendKeys("Зубенко Михаил Петрович");
        return this;
    }
    //Ввод адреса
    public Zakaz2 setAdres2() {
        Adres2.sendKeys("пл Ленина"+ Keys.TAB);
        return this;
    }
    // Ввод дома
    public Zakaz2 SetDom1() {
        Dom1.click();
        return this;
    }
    // Ввод дома
    public Zakaz2 SetDom2() {
        Dom2.clear();
        return this;
    }
    // Ввод дома
    public Zakaz2 SetDom3() {
        Dom3.sendKeys("д. 10" + Keys.TAB);
        return this;
    }
    // Оформить заказ
    public Zakaz2 SetOformit2() {
        Oformit2.sendKeys("д. 10" + Keys.TAB);
        return this;
    }

}