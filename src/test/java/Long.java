import DZ.Long2;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.assertj.core.api.SoftAssertions;
import java.util.concurrent.TimeUnit;
import org.junit.Test;

public class Long {

    public static WebDriver driver;
    private Long2 Long2_2;

    @Test
    public void case1 () {

        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        Long2_2 = new Long2(driver);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //Поиск Лонгслива
        SoftAssertions softAssertions = new SoftAssertions();
        driver.get("https://homebrandofficial.ru/wear");
        Long2_2.setPoisk();
        Long2_2.setButtonSearch();

        // Проверка названия
        Long2_2.resultNameCheck(softAssertions, "Лонгслив White&Green");

        // Проверка на цену
        Long2_2.resultPriceCheck(softAssertions, "2 800");

        //Проверка на количество
        Long2_2.searchResNumCheck(softAssertions,"1");
    }


    }

