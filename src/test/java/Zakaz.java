import DZ.Zakaz2;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

public class Zakaz {

    public static WebDriver driver;
    private Zakaz2 Zakaz2_2;

    @Test
    public void case2 () {

        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        Zakaz2_2 = new Zakaz2(driver);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        SoftAssertions softAssertions = new SoftAssertions();
        driver.get("https://homebrandofficial.ru/wear");

        // Нажать на товар с названием “Футболка Оversize” и добавить в корзину
        Zakaz2_2.setTshort();
        Zakaz2_2.setKorzina();
        Zakaz2_2.setDobavit();

        // Нажать ОФОРМИТЬ ЗАКАЗ
        Zakaz2_2.setOformit();

        // Ввод ФИО и адреса
        Zakaz2_2.setFIO1();
        driver.findElement(By.id("input_1630305196291")).sendKeys("Томск ул. Ленина 10 кв. 1");

        //Ввод номера телефона
        Zakaz2_2.setNumber();

        // Выбор города
       Zakaz2_2.setGorodclick();
       Zakaz2_2.setGorodclear();
       Zakaz2_2.setGorod1();
       Zakaz2_2.setGorod2();

        // Ввод ФИО
        Zakaz2_2.setFIO2();


        // Ввод адреса
        Zakaz2_2.setAdres2();

        // Ввод дома
        Zakaz2_2.SetDom1();
        Zakaz2_2.SetDom2();
        Zakaz2_2.SetDom3();

        //На этом моменте у меня перестае отрабатываться код и система стопорится на выборе дома чтобы я не вводил и отказывается
        // нажимать кнопку оформить сделал. Я сделал все что мог, дальше проверку без этой кнопки делать бесмыссленно,
        // но хотя бы получилось в первой части



        // Нажать н кнопку ОФОРМИТЬ ЗАКАЗ
        Zakaz2_2.SetOformit2();
     // driver.findElement(By.xpath("//button[@class='t-submit'][text()='ОФОРМИТЬ ЗАКАЗ']")).click();


        //Проверка на некорректный номер 1
        By number1 = By.xpath("//div[text()='Укажите, пожалуйста, корректный номер телефона']");
        WebElement numberone = driver.findElement(number1);
        String numbername1 = "Укажите, пожалуйста, корректный номер телефона";
        softAssertions.assertThat(numberone.getText()).as("Неправильная проверка теста №1").isEqualToIgnoringCase(numbername1);

        //Проверка на некорректный номер 2
        By number2 = By.xpath("//p[text()='Укажите, пожалуйста, корректный номер телефона']");
        WebElement numbertwo = driver.findElement(number1);
        String numbername2 = "Укажите, пожалуйста, корректный номер телефона";
        softAssertions.assertThat(numberone.getText()).as("Неправильная проверка теста №2").isEqualToIgnoringCase(numbername2);
        softAssertions.assertAll();

    }
}
