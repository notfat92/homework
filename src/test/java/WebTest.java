import DZ.Page.ChoiseTshort;
import DZ.Page.FullDesign;
import DZ.Page.HomePage;

import DZ.Page.SendToBasket;
import org.assertj.core.api.SoftAssertions;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class WebTest {

    private WebDriver driver;
    private HomePage homePage;
    private ChoiseTshort Tshort;
    private SendToBasket tshortBusket;
    private FullDesign Design_Full;

    @Before
    public void init() {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        homePage = new HomePage(driver);
        Tshort = new ChoiseTshort(driver);
        tshortBusket = new SendToBasket(driver);
        Design_Full = new FullDesign(driver);
    }

    @Test
    public void testErrorMassage() {
        SoftAssertions softAssertions = new SoftAssertions();
        homePage.openHomePage()
                .loadWait()
                .enterSearchText("Лонгслив White&Green")
                .clickSearch()
                .checkName("Лонгслив White&Green", softAssertions);
        softAssertions.assertAll();

    }
    @Test
    public void testErrorMassage2() {
        SoftAssertions softAssertions = new SoftAssertions();
        Tshort.openHomePage()
                .loadWait()
                .clickTshortOverSize();
        tshortBusket.loadWait()
                .setBasket();
        Tshort.setClickBusket()
                .setCheckoutTshort();
        Design_Full.loadWait()
                .setFullName()
                .setAdress()
                .setNumber()
                .setCityclick()
                .setCityclear()
                .setCity1()
                .setCity1()
                .setFullName2()
                .setAdress2()
                .setHouse1()
                .setHouse2()
                .setHouse3()
                .setMainOrder()
                .checkNumber1("Укажите, пожалуйста, корректный номер телефона", softAssertions);

        Design_Full.checkNumber2("Укажите, пожалуйста, корректный номер телефона", softAssertions);
        softAssertions.assertAll();

    }

}
